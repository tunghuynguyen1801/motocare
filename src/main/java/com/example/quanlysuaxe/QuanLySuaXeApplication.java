package com.example.quanlysuaxe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuanLySuaXeApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuanLySuaXeApplication.class, args);
    }

}
