package com.example.quanlysuaxe.repository;

import com.example.quanlysuaxe.dto.requestDTO.UserRequestDTO;
import com.example.quanlysuaxe.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<User, Integer>, UserRepositoryCustom {
    @Query(value = "select * From users where users.phone = :phone and users.status = :status ", nativeQuery = true)
    Optional<User> findByPhone(String phone, Integer status);

    Optional<User> findUserByPhone(String phone);

    @Query(value = "select * From users where users.id = :id and users.status = :status", nativeQuery = true)
    User findUserByIdAndStatus(Integer id, Integer status);
}
