package com.example.quanlysuaxe.repository.impl;

import com.example.quanlysuaxe.dto.requestDTO.UserRequestDTO;
import com.example.quanlysuaxe.entity.User;
import com.example.quanlysuaxe.repository.UserRepositoryCustom;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class UserRepositoryCustomImpl implements UserRepositoryCustom {
    @Autowired
    private EntityManager entityManager;

    @Override
    public List<User> filterUsers(UserRequestDTO dto) {
        StringBuilder sql = new StringBuilder();
        sql.append("select * from users ");
        sql.append("where users.name like :userName ");
        sql.append("and (users.address like :address or users.address is null) ");
        sql.append("and (-1 = :status or users.status = :status) ");

        NativeQuery query = (NativeQuery) entityManager.createNativeQuery(sql.toString(), User.class);
        query.setParameter("userName", "%" + dto.getName() + "%");
        query.setParameter("address", "%" + dto.getAddress() + "%");
        query.setParameter("status", dto.getStatus());

        return query.list();
    }

    @Override
    public List<User> findUsersLoyalByStoreId(Integer storeId) {
        StringBuilder sql = new StringBuilder();
        sql.append("select * from users ");
        sql.append("where id in ");
        sql.append("(SELECT bookings.user_id FROM bookings ");
        sql.append("where status = 1 and bookings.store_id = :storeId ");
        sql.append("group by bookings.user_id ");
        sql.append("having count(user_id) >=3)");

        NativeQuery query = (NativeQuery) entityManager.createNativeQuery(sql.toString(), User.class);
        query.setParameter("storeId", storeId);
        List<User> result = query.getResultList();
        return result;
    }
}
