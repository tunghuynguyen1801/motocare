package com.example.quanlysuaxe.repository.impl;

import com.example.quanlysuaxe.dto.requestDTO.StoreRequestDTO;
import com.example.quanlysuaxe.dto.responseDTO.ServiceDTO;
import com.example.quanlysuaxe.entity.*;
import com.example.quanlysuaxe.repository.StoreRepository;
import com.example.quanlysuaxe.repository.StoreRepositoryCustom;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class StoreRepositoryCustomImpl implements StoreRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Booking> findBookingsByStoreId(Integer storeId, String startDate, String endDate) {
        StringBuilder sql = new StringBuilder();
        sql.append("select bookings.* ");
        sql.append("from bookings ");
        sql.append("where status = 0 and ");
        sql.append("store_id = :storeId and ");
        sql.append("time_repair >= :startDate and ");
        sql.append("time_repair <= :endDate ");

        NativeQuery query = (NativeQuery) entityManager.createNativeQuery(sql.toString());
        query.addEntity(Booking.class);
        query.setParameter("storeId", storeId);
        query.setParameter("startDate", (startDate == null || startDate.equals("")) ? "0000-01-01" : startDate);
        query.setParameter("endDate", (endDate == null || startDate.equals("")) ? "9999-12-31" : endDate);


        List<Booking> result = query.list();
        return result;
    }

    @Override
    public List<ServiceDTO> findServicesByBookingId(Integer bookingId) {
        StringBuilder sql = new StringBuilder();
        sql.append("select services.* ");
        sql.append("from services ");
        sql.append("left join booking_service on services.id = booking_service.service_id ");
        sql.append("left join bookings on bookings.id = booking_service.booking_id ");
        sql.append("where bookings.id = :bookingId ");

        Query query = entityManager.createNativeQuery(sql.toString(), Service.class);
        query.setParameter("bookingId", bookingId);
        List<Service> result = query.getResultList();
        List<ServiceDTO> serviceList = new ArrayList<>();
        for (Service service : result) {
            ServiceDTO service1 = new ServiceDTO();
            service1.setName(service.getName());
            service1.setPrice(service.getPrice());
            serviceList.add(service1);
        }
        System.out.println(serviceList.toArray().toString());
        return serviceList;
    }

    @Override
    public List<Booking> filterBookings(String userName, String phone, Integer storeId) {
        StringBuilder sql = new StringBuilder();
        sql.append("select bookings.* ");
        sql.append("from bookings ");
        sql.append("where status = 0 and ");
        sql.append("store_id = :storeId and ");
        sql.append("user_name like :userName and ");
        sql.append("(:phone is null or phone_user = :phone) ");

        NativeQuery query = (NativeQuery) entityManager.createNativeQuery(sql.toString(), Booking.class);
        query.setParameter("userName", userName == null? "%%":"%"+userName+"%");
        query.setParameter("storeId",storeId);
        query.setParameter("phone", phone);
        return query.getResultList();
    }
}
