package com.example.quanlysuaxe.repository.impl;

import com.example.quanlysuaxe.entity.Service;
import com.example.quanlysuaxe.repository.ServiceRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class ServiceRepositoryCustomImpl implements ServiceRepositoryCustom {
    @Autowired
    private EntityManager entityManager;

    @Override
    public Boolean deleteServiceStore( Integer storeId ,Integer serviceId ) {
        StringBuffer sql = new StringBuffer();
        sql.append("Delete  FROM motospa.store_service ");
        sql.append("where service_id = :serviceId AND store_id = :storeId");

        Query query = entityManager.createNativeQuery(sql.toString());
        query.setParameter("storeId", storeId);
        query.setParameter("serviceId", serviceId);
        int a = query.executeUpdate();

        if (a > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<Service> findServicesByStoreId(Integer storeId) {
        StringBuilder sql = new StringBuilder();
        sql.append("select services.* from services ");
        sql.append("left join store_service on services.id = store_service.service_id ");
        sql.append("where store_service.store_id = :storeId");

        Query query = entityManager.createNativeQuery(sql.toString(), Service.class);
        query.setParameter("storeId", storeId);
        return query.getResultList();
    }


}
