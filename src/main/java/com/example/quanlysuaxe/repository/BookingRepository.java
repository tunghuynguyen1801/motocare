package com.example.quanlysuaxe.repository;

import com.example.quanlysuaxe.entity.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Integer> {
    @Query(value = "select * from bookings where bookings.user_id = :userId", nativeQuery = true)
    List<Booking> findBookingByUserId(Integer userId);
}
