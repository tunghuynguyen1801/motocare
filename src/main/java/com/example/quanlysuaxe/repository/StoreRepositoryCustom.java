package com.example.quanlysuaxe.repository;

import com.example.quanlysuaxe.dto.requestDTO.StoreRequestDTO;
import com.example.quanlysuaxe.dto.responseDTO.ServiceDTO;
import com.example.quanlysuaxe.entity.*;

import java.util.List;

public interface StoreRepositoryCustom {
    List<Booking> findBookingsByStoreId(Integer storeId, String startTime, String endTime);

    List<ServiceDTO> findServicesByBookingId(Integer bookingId);

    List<Booking> filterBookings(String userName, String phone, Integer storeId);
}
