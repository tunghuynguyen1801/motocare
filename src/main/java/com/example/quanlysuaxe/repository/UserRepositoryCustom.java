package com.example.quanlysuaxe.repository;

import com.example.quanlysuaxe.dto.requestDTO.UserRequestDTO;
import com.example.quanlysuaxe.entity.User;

import java.util.List;

public interface UserRepositoryCustom {
    List<User> filterUsers(UserRequestDTO dto);

    List<User> findUsersLoyalByStoreId(Integer storeId);
}
