package com.example.quanlysuaxe.repository;

import com.example.quanlysuaxe.dto.responseDTO.ServiceDTO;
import com.example.quanlysuaxe.entity.Booking;
import com.example.quanlysuaxe.entity.Store;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StoreRepository extends JpaRepository<Store, Integer>, StoreRepositoryCustom {
    @Query(value = "select * from stores where stores.user_id = :userId", nativeQuery = true)
    List<Store> findStoresByUserId(Integer userId);

    @Query(value = "select *from stores as s where  s.id = :storeID ", nativeQuery = true)
    Optional<Store> findByIdStore(Integer storeID);
}
