package com.example.quanlysuaxe.repository;

import com.example.quanlysuaxe.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Integer> {
    @Query(value = "select * from comments where comments.store_id = :storeId", nativeQuery = true)
    List<Comment> findCommentsByStore(Integer storeId);
}
