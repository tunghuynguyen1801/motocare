package com.example.quanlysuaxe.repository;

import com.example.quanlysuaxe.entity.Service;

import java.util.List;

public interface ServiceRepositoryCustom {
    List<Service> findServicesByStoreId(Integer StoreId);

    Boolean deleteServiceStore(Integer storeId,Integer serviceId) ;
}
