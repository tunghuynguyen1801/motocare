package com.example.quanlysuaxe.repository;

import com.example.quanlysuaxe.entity.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Integer>, ServiceRepositoryCustom {
    void deleteById(Integer id);

    Optional<Service> findServiceById(Integer serviceId);

    @Query(value = "select * from services where name like %:name%", nativeQuery = true)
    List<Service> findServicesByName(String name);
}
