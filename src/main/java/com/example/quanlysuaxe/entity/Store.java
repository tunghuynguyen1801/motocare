package com.example.quanlysuaxe.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "stores")
public class Store {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false,cascade = CascadeType.PERSIST)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @Column(name = "address", nullable = false, length = 100)
    private String address;

    @Column(name = "phone", nullable = false, length = 45)
    private String phone;

    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "created_time")
    private Timestamp createdTime;

    @Column(name = "updated_by", length = 50)
    private String updatedBy;

    @Column(name = "update_time")
    private Timestamp updateTime;

    @Column(name = "status")
    private Integer status;

    @ManyToMany
    @JoinTable(
            name = "store_service",
            joinColumns = @JoinColumn(name = "store_id"),
            inverseJoinColumns = @JoinColumn(name = "service_id")
    )
    private Set<Service> services;

    @OneToMany(mappedBy = "store", cascade = CascadeType.PERSIST)
    private List<Booking> bookings;

    @OneToMany(mappedBy = "store1",cascade = CascadeType.PERSIST)
    private List<Comment> comments;
}