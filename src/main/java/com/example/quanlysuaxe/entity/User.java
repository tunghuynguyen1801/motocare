package com.example.quanlysuaxe.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @Column(name = "gender", length = 20)
    private String gender;

    @Column(name = "phone", nullable = false, length = 20)
    private String phone;

    @Column(name = "address", length = 100)
    private String address;

    @Column(name = "age")
    private Integer age;

    @Column(name = "password", nullable = false, length = 100)
    private String password;

    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "created_time")
    private Timestamp createdTime;

    @Column(name = "updated_by", length = 50)
    private String updatedBy;

    @Column(name = "updated_time")
    private Timestamp updatedTime;

    @Column(name = "status", nullable = false)
    private Integer status;
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Store> stores;

    public User(String name, String phone, String password,
                String address, int age, String gender) {
        this.name = name;
        this.phone = phone;
        this.password = password;
        this.address = address;
        this.age = age;
        this.gender = gender;
        this.status = 1;
    }
}