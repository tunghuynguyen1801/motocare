package com.example.quanlysuaxe.entity;

import com.example.quanlysuaxe.dto.responseDTO.ServiceDTO;
import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "bookings")
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "store_id", nullable = false)
    private Store store;

    @Column(name = "time_repair", nullable = false)
    private Timestamp timeRepair;

    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "created_time")
    private Timestamp createdTime;

    @Column(name = "updated_by", length = 50)
    private String updatedBy;

    @Column(name = "updated_time")
    private Timestamp updatedTime;

    @Column(name = "status")
    private Integer status;

    @Column(name = "number_plate", length = 30)
    private String numberPlate;

    @Column(name = "user_name", length = 50)
    private String userName;

    @Column(name = "phone_user", length = 20)
    private String phoneUser;

    @ManyToMany(cascade = CascadeType.DETACH)
    @JoinTable(
            name = "booking_service",
            joinColumns = @JoinColumn(name = "booking_id"),
            inverseJoinColumns = @JoinColumn(name = "service_id")
    )
    private Set<Service> services;
}