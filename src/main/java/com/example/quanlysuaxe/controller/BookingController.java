package com.example.quanlysuaxe.controller;

import com.example.quanlysuaxe.dto.requestDTO.BookingRequestDTO;
import com.example.quanlysuaxe.dto.responseDTO.BookingDTO;
import com.example.quanlysuaxe.message.CommonController;
import com.example.quanlysuaxe.message.Const;
import com.example.quanlysuaxe.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(path = "/api/v1/bookings")
public class BookingController extends CommonController {
    @Autowired
    private BookingService bookingService;

    @PostMapping("/{store-id}/users/{user-id}")
    public ResponseEntity<?> addBooking(@RequestBody BookingRequestDTO dto,
                                           @PathVariable(name = "user-id") Integer userId,
                                           @PathVariable(name = "store-id") Integer storeId) {
        try {
            String result = bookingService.addBooking(dto, userId, storeId);
            if (result.equals("FAIL")) {
                return toExceptionResult("Insert booking false", Const.API_RESPONSE.RETURN_CODE_ERROR);
            }
            return toSuccessResult("Insert booking successfully");
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);
        }
    }

    @DeleteMapping("/{booking-id}/deletion")
    public ResponseEntity<?> deleteBooking(@PathVariable(name = "booking-id") Integer id) {
        try {
            String result = bookingService.deleteBooking(id);
            if (result.equals("FAIL")) {
                return toExceptionResult("Delete booking false", Const.API_RESPONSE.RETURN_CODE_ERROR);
            }
            return toSuccessResult("Delete booking successfully");
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);
        }
    }

    @GetMapping("/{store-id}/filterBookings")
    public ResponseEntity<?> filterBookings(@PathVariable(name = "store-id") Integer storeId,
                                            @RequestParam(name = "user", required = false, defaultValue = "") String user,
                                            @RequestParam(name = "phone", required = false) String phone) {
        try {
            List<BookingDTO> result = bookingService.filterBookings(user, phone, storeId);
            if (result.size() == 0){
                return toExceptionResult("List Booking empty",Const.API_RESPONSE.RETURN_CODE_ERROR);
            }
            return toSuccessResult(result);
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);
        }
    }


}
