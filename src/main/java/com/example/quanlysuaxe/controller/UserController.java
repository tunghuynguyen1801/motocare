package com.example.quanlysuaxe.controller;

import com.example.quanlysuaxe.dto.requestDTO.CommentRequestDTO;
import com.example.quanlysuaxe.dto.requestDTO.PasswordRequest;
import com.example.quanlysuaxe.dto.requestDTO.StoreRequestDTO;
import com.example.quanlysuaxe.dto.requestDTO.UserRequestDTO;
import com.example.quanlysuaxe.dto.responseDTO.*;
import com.example.quanlysuaxe.message.CommonController;
import com.example.quanlysuaxe.message.Const;
import com.example.quanlysuaxe.service.CommentService;
import com.example.quanlysuaxe.service.StoreService;
import com.example.quanlysuaxe.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1")
public class UserController extends CommonController {
    @Autowired
    private UserService userService;
    
    @Autowired
    private CommentService commentService;

    @PostMapping("/users/registration")
    public ResponseEntity<?> addNewUser(@RequestBody UserRequestDTO newUser) {
        try {
            UserDTO result = userService.addUser(newUser);
            if (result == null) {
                return toExceptionResult("Registration fail", Const.API_RESPONSE.RETURN_CODE_ERROR);
            }
            return toSuccessResult(result);
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);
        }
    }

    @PutMapping("/users/{id}/update")
    public ResponseEntity<?> updateUserInfo(@PathVariable(name = "id") Integer id,
                                            @RequestBody UserRequestDTO user) {
        try {
            String result = userService.updateUserInfo(id, user);
            if (result.equals("FAIL")) {
                return toExceptionResult("Update fail", Const.API_RESPONSE.RETURN_CODE_ERROR);
            }
            return toSuccessResult("Update Successfully");
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);
        }

    }

    @PostMapping("/users/{user-id}/password")
    public ResponseEntity<?> updatePassword(@Valid @RequestBody PasswordRequest dto,
                                            @PathVariable(name = "user-id") Integer userId) {
        try {
            String result = userService.updatePassword(dto.getNewPassword(),userId, dto.getPasswordSaved());
            if (result.equals("FAIL")){
                return toExceptionResult("Change password fail", Const.API_RESPONSE.RETURN_CODE_ERROR);
            }
            return toSuccessResult("Change password successfully");
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);
        }
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<?> getUserById(@PathVariable(name = "id") Integer id) {
        try {
            UserDTO result = userService.findUserById(id);
            if (result == null) {
                return toExceptionResult("Not found", Const.API_RESPONSE.RETURN_CODE_ERROR);
            }
            return toSuccessResult(result);
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);
        }
    }

    @GetMapping("/users")
    public ResponseEntity<?> getUsersByPhone(@RequestParam(name = "phone", required = false) String phoneNumber) {
        try {
            UserDTO result = userService.findUserByPhone(phoneNumber);
            if (result == null) {
                return toExceptionResult("Not found", Const.API_RESPONSE.RETURN_CODE_ERROR);
            }
            return toSuccessResult(result);
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);
        }
    }

    @GetMapping("/users/{id}/bookings")
    public ResponseEntity<?> findBookingsByUser(@PathVariable(name = "id") Integer id) {
        try {
            List<BookingDTO> result = userService.findBookingsByUserId(id);
            if (result.size() == 0) {
                toExceptionResult("List is empty", Const.API_RESPONSE.RETURN_CODE_ERROR);
            }
            return toSuccessResult(result);
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);
        }
    }

    @GetMapping("/users/{id}/bills")
    public ResponseEntity<?> findBillsByUser(@PathVariable(name = "id") Integer id) {
        try {
            List<BillDTO> result = userService.findBillsByUserId(id);
            if (result.size() == 0) {
                toExceptionResult("List is empty", Const.API_RESPONSE.RETURN_CODE_ERROR);
            }
            return toSuccessResult(result);
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);
        }
    }

    @PostMapping("/users/{user-id}/comments")
    public ResponseEntity<?> insertComment(@Valid @PathVariable(name = "user-id") Integer userId,
                                           @RequestParam(name = "storeId") Integer storeId,
                                           @RequestBody CommentRequestDTO dto) {
        try {
            String result = commentService.insertComment(userId, storeId, dto);
            if (result.equals("SUCCESS")) {
                return toSuccessResult(result);
            }
            return toExceptionResult(result, Const.API_RESPONSE.RETURN_CODE_ERROR);
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);
        }
    }

}
