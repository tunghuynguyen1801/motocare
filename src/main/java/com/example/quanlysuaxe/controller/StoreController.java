package com.example.quanlysuaxe.controller;

import com.example.quanlysuaxe.dto.requestDTO.ServiceRequestDTO;
import com.example.quanlysuaxe.dto.requestDTO.StoreRequestDTO;
import com.example.quanlysuaxe.dto.responseDTO.*;
import com.example.quanlysuaxe.message.CommonController;
import com.example.quanlysuaxe.message.Const;
import com.example.quanlysuaxe.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(path = "/api/v1/stores")
public class StoreController extends CommonController {
    @Autowired
    private StoreService storeService;
    @Autowired
    private ServiceService serviceService;
    @Autowired
    private BookingService bookingService;
    @Autowired
    private UserService userService;
    @Autowired
    private CommentService commentService;

    //Hiển thị các đơn đặt
    @GetMapping("/{storeId}/bookings")
    public ResponseEntity<?> findBookingsByTime(@PathVariable(name = "storeId") Integer storeId,
                                                @RequestParam(required = false, defaultValue = "") Map<String, String> params) {
        try {
            String startDate = params.get("startDate");
            String endDate = params.get("endDate");
            List<BookingDTO> list = storeService.findBookingsByStoreId(storeId, startDate, endDate);
            if (list.isEmpty()) {
                return toExceptionResult("List is empty", Const.API_RESPONSE.RETURN_CODE_ERROR);
            }
            return toSuccessResult(list);
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);
        }
    }

    //Xác nhận hoàn thành bookings
    @PutMapping("/{store-id}/bookings/{booking-id}")
    public ResponseEntity<?> confirmBooking(@PathVariable(name = "store-id") Integer storeId,
                                            @PathVariable(name = "booking-id") Integer bookingId) {
        try {
            String result = bookingService.confirmBooking(storeId, bookingId);
            if (result.equals("Fail")) {
                return toExceptionResult("Confirm bookings false", Const.API_RESPONSE.RETURN_CODE_ERROR);
            }
            return toSuccessResult("Confirm bookings success");
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);
        }
    }

    //Khách hàng thân thiết
    @GetMapping("/{storeId}/users-loyal")
    public ResponseEntity<?> showUserLoyal(@PathVariable(name = "storeId") Integer storeId) {
        try {
            List<UserDTO> userList = userService.findAllUserLoyalByStoreId(storeId);
            if (userList != null) {
                return toSuccessResult(userList);
            }
            return toExceptionResult("Not exist", Const.API_RESPONSE.RETURN_CODE_ERROR);
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);
        }
    }

    //Get cửa hàng theo id
    @GetMapping("/{store_id}")
    public ResponseEntity<?> getStores(@PathVariable(name = "store_id") Integer storeID) {
        try {
            StoreDTO result = storeService.getStoreById(storeID);
            if (result == null){
                return toExceptionResult("Store is empty",Const.API_RESPONSE.RETURN_CODE_ERROR);
            }
            return toSuccessResult(result);
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);

        }
    }

    // Update thông tin cửa hàng
    @PutMapping("{id}/services")
    public ResponseEntity<?> updateInformationStore(@PathVariable(name = "id") Integer storeID,
                                                    @RequestBody StoreRequestDTO storeRequestDTO) {
        try {
            StoreDTO result = storeService.editStore(storeRequestDTO, storeID);
            if (result == null){
                return toExceptionResult("Update fail",Const.API_RESPONSE.RETURN_CODE_ERROR);
            }
            return toSuccessResult(result);
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);
        }
    }

    //Xóa cửa hàng
    @DeleteMapping("{id}/deleting")
    public ResponseEntity<?> deleteStore(@PathVariable(name = "id") Integer storeID) {
        try {
            return toSuccessResult(storeService.deleteStore(storeID));
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);
        }
    }

    //Danh sách dịch vụ của cửa hàng
    @GetMapping("/listSevrices/{storeId}")
    public ResponseEntity<?> getListService(@PathVariable(name = "storeId") Integer storeId) {
        try {
            Set<ServiceDTO> result = serviceService.findServicesByStoreId(storeId);
            if (result.isEmpty()){
                return toExceptionResult("List service is empty", Const.API_RESPONSE.RETURN_CODE_ERROR);
            }
            return toSuccessResult(result);
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);
        }
    }

    //Thêm dịch vụ của cửa hàng
    @PostMapping("/additionService/{storeId}")
    public ResponseEntity<?> addServiceByStore(@PathVariable(name = "storeId") Integer storeId,
                                               @RequestBody ServiceRequestDTO serviceResquestDTO) {
        try {
            String result = storeService.addNewService(serviceResquestDTO, storeId);
            if (result.equals("Fail")){
                return toExceptionResult("Service already exist",Const.API_RESPONSE.RETURN_CODE_ERROR);
            }
            return toSuccessResult("Add service successfully");
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);
        }

    }

    //Xóa dịch vụ
    @DeleteMapping("/{storeid}/deleting/{serviceId}")
    public ResponseEntity<?> deletingService(@PathVariable(name = "serviceId") Integer serviceId
                                            ,@PathVariable(name = "storeid") Integer storeId) {
        try {
            return toSuccessResult(storeService.deleteService(storeId,serviceId));
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), "400");
        }
    }

    @PutMapping("/{store-id}/updating/{service}")
    public ResponseEntity<?> updateService(@PathVariable(name = "service") Integer serviceId,
                                           @PathVariable(name = "store-id") Integer storeId,
                                           @RequestBody ServiceRequestDTO serviceResquestDTO) {
        try {
            String result = storeService.updateService(serviceResquestDTO, serviceId, storeId);
            if (result.equals("Fail")){
                return toExceptionResult("You not changed", Const.API_RESPONSE.RETURN_CODE_ERROR);
            }
            return toSuccessResult("Update successfully");
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);
        }
    }

    @GetMapping("/{store_id}/listComment")
    public ResponseEntity<?> getListComment(@PathVariable(name = "store_id") Integer storeId) {
        try {
            List<CommentDTO> result = commentService.getListComments(storeId);
            if (result.size() == 0){
                return toExceptionResult("List comment is empty",Const.API_RESPONSE.RETURN_CODE_ERROR);
            }
            return toSuccessResult(result);
        } catch (Exception e) {
            return toExceptionResult(e.getMessage(), Const.API_RESPONSE.RETURN_CODE_ERROR);

        }
    }
}
