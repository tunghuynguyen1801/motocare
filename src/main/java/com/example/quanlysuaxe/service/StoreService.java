package com.example.quanlysuaxe.service;

import com.example.quanlysuaxe.dto.requestDTO.ServiceRequestDTO;
import com.example.quanlysuaxe.dto.requestDTO.StoreRequestDTO;
import com.example.quanlysuaxe.dto.responseDTO.*;
import com.example.quanlysuaxe.entity.Store;

import java.util.List;


public interface StoreService {
    List<BookingDTO> findBookingsByStoreId(Integer storeId, String startDate, String endDate);
    StoreDTO getStoreById(Integer storeId);
    StoreDTO editStore(StoreRequestDTO storeDTO, Integer storeId);
    String deleteStore(Integer id);
    String addNewService(ServiceRequestDTO serviceRequestDTO, Integer storeId);
    String deleteService(Integer storeId, Integer serviceId);
    String updateService(ServiceRequestDTO serviceResquestDTO, Integer serivceId, Integer storeId);
    List<StoreDTO> convertStoresDTO(List<Store> stores);
    StoreDTO convertStore(Store store);

}
