package com.example.quanlysuaxe.service;

import com.example.quanlysuaxe.dto.requestDTO.BookingRequestDTO;
import com.example.quanlysuaxe.dto.responseDTO.BookingDTO;
import com.example.quanlysuaxe.entity.Booking;

import java.text.ParseException;
import java.util.List;

public interface BookingService {
    String addBooking(BookingRequestDTO dto, Integer userId, Integer storeId) throws ParseException;
    String deleteBooking(Integer id);
    String confirmBooking(Integer storeId, Integer bookingId);
    List<BookingDTO> convertBookings(List<Booking> bookings);
    List<BookingDTO> filterBookings(String userName ,String phoneUser, Integer storeId);
}
