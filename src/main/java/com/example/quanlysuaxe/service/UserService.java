package com.example.quanlysuaxe.service;

import com.example.quanlysuaxe.dto.requestDTO.UserRequestDTO;
import com.example.quanlysuaxe.dto.responseDTO.*;
import com.example.quanlysuaxe.entity.User;

import java.util.List;

public interface UserService {
    //-----------Nhom chuc nang cho User--------------
    UserDTO addUser(UserRequestDTO userRequestDTO);
    String updateUserInfo(Integer userId, UserRequestDTO userRequestDTO);
    String updatePassword(String password, Integer userId, String passwordSaved);

    //-----------Nhom chuc nang cho admin-------------
    //-----Tim kiem----------
    UserDTO findUserById(Integer id);
    UserDTO findUserByPhone(String phone);
    List<BillDTO> findBillsByUserId(Integer userId);
    List<BookingDTO> findBookingsByUserId(Integer userId);
    List<UserDTO> findAllUserLoyalByStoreId(Integer storeId);

    //-----Bo loc------------
    List<UserDTO> filterUsers(UserRequestDTO userRequestDTO);

    //-----Nhom ham ho tro -----------
    UserDTO convertUser(User user);
    List<UserDTO> convertUsers(List<User> users);
}
