package com.example.quanlysuaxe.service;

import com.example.quanlysuaxe.dto.responseDTO.ServiceDTO;
import com.example.quanlysuaxe.entity.Service;

import java.text.ParseException;
import java.util.Set;

public interface ServiceService {
    Set<ServiceDTO> findServicesByStoreId(Integer storeId);
    Set<ServiceDTO> convertServices(Set<Service> services);
    Set<Service> serviceDTOConvertService(Set<ServiceDTO> dtos) throws ParseException;
}
