package com.example.quanlysuaxe.service.impl;

import com.example.quanlysuaxe.dto.requestDTO.CommentRequestDTO;
import com.example.quanlysuaxe.dto.responseDTO.CommentDTO;
import com.example.quanlysuaxe.entity.Comment;
import com.example.quanlysuaxe.entity.Store;
import com.example.quanlysuaxe.entity.User;
import com.example.quanlysuaxe.repository.CommentRepository;
import com.example.quanlysuaxe.repository.StoreRepository;
import com.example.quanlysuaxe.repository.UserRepository;
import com.example.quanlysuaxe.service.CommentService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private StoreRepository storeRepository;

    @Override
    public String insertComment(int userId, int storeId, CommentRequestDTO dto) {
        Optional<User> userSaved = userRepository.findById(userId); //cho vào commonRequestDTO
        Optional<Store> storeSaved = storeRepository.findById(storeId); //như trên
        if (!userSaved.isPresent() || !storeSaved.isPresent()) {
            return "Fail";
        }
        User user = userSaved.get();
        Store store = storeSaved.get();
        Comment newComment = getComment(dto, user, store);

        commentRepository.save(newComment);
        return "SUCCESS";
    }

    @NotNull
    private Comment getComment(CommentRequestDTO dto, User user, Store store) {
        Comment newComment = new Comment();
        newComment.setId(0);
        newComment.setUser(user);
        newComment.setContent(dto.getContent());
        newComment.setStore1(store);
        Timestamp currentDate = new Timestamp(new Date().getTime());
        newComment.setDate(currentDate);
        newComment.setStar(dto.getStar());
        newComment.setCreatedBy(user.getName());
        newComment.setCreatedTime(currentDate);
        newComment.setUpdatedBy(user.getName());
        newComment.setUpdatedTime(currentDate);
        newComment.setStatus(1);
        return newComment;
    }

    @Override
    public List<CommentDTO> getListComments(Integer storeId) {
        return this.convertComments(commentRepository.findCommentsByStore(storeId));
    }


    @Override
    public List<CommentDTO> convertComments(List<Comment> comments) {
        return comments.stream().map(comment -> new CommentDTO(comment.getUser().getName(),
                comment.getStar(), comment.getContent(), comment.getCreatedTime()
        )).collect(Collectors.toList());
    }
}