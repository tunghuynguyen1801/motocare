package com.example.quanlysuaxe.service.impl;

import com.example.quanlysuaxe.dto.responseDTO.ServiceDTO;
import com.example.quanlysuaxe.formatter.ServiceFormatter;
import com.example.quanlysuaxe.repository.ServiceRepository;
import com.example.quanlysuaxe.service.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ServiceImpl implements ServiceService {
    @Autowired
    private ServiceRepository serviceRepository;
    @Autowired
    private ServiceFormatter formatter;


    @Override
    public Set<ServiceDTO> findServicesByStoreId(Integer storeId) {
        Set<com.example.quanlysuaxe.entity.Service> result = new HashSet<>(serviceRepository.findServicesByStoreId(storeId));
        return this.convertServices(result);
    }

    @Override
    public Set<ServiceDTO> convertServices(Set<com.example.quanlysuaxe.entity.Service> services) {
        Set<ServiceDTO> result = services.stream().map(service -> new ServiceDTO(
                service.getId(), service.getName(), service.getPrice(), service.getTypeVehicle()
        )).collect(Collectors.toSet());
        return result;
    }

    @Override
    public Set<com.example.quanlysuaxe.entity.Service> serviceDTOConvertService(Set<ServiceDTO> dtos) throws ParseException {
        Set<com.example.quanlysuaxe.entity.Service> result = new HashSet<>();
        com.example.quanlysuaxe.entity.Service service;
        Iterator<ServiceDTO> listService = dtos.iterator();
        while (listService.hasNext()) {
            service = formatter.parse(String.valueOf(listService.next().getServiceId()),Locale.getDefault());
            result.add(service);
        }
        return result;
    }
}
