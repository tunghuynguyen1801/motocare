package com.example.quanlysuaxe.service.impl;

import com.example.quanlysuaxe.dto.responseDTO.BillDTO;
import com.example.quanlysuaxe.entity.Booking;
import com.example.quanlysuaxe.repository.StoreRepository;
import com.example.quanlysuaxe.service.BillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BillServiceImpl implements BillService {
    @Autowired
    private StoreRepository storeRepository;

    @Override
    public List<BillDTO> convertBills(List<Booking> bookings) {
        List<BillDTO> result = new ArrayList<>();
        for (Booking b : bookings) {
            if (b.getStatus().equals(1)) {
                result.add(new BillDTO(b.getUserName(), b.getPhoneUser(),
                        b.getStore().getName(), b.getTimeRepair(), b.getNumberPlate(),
                        storeRepository.findServicesByBookingId(b.getId()),
                        b.getCreatedTime()));
            }
        }
        return result;
    }
}
