package com.example.quanlysuaxe.service.impl;

import com.example.quanlysuaxe.service.UserService;
import com.example.quanlysuaxe.dto.requestDTO.UserRequestDTO;
import com.example.quanlysuaxe.dto.responseDTO.*;
import com.example.quanlysuaxe.entity.User;
import com.example.quanlysuaxe.repository.BookingRepository;
import com.example.quanlysuaxe.repository.UserRepository;
import com.example.quanlysuaxe.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private BillService billService;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private StoreService storeService;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Override
    public UserDTO addUser(UserRequestDTO userRequestDTO) {
        Optional<User> checkPhoneUser = userRepository.findUserByPhone(userRequestDTO.getPhone());
        if (!checkPhoneUser.isPresent()) {
            return null;
        } else {
            User newUser = new User();
            newUser.setName(userRequestDTO.getName());
            newUser.setGender(userRequestDTO.getGender());
            newUser.setPhone(userRequestDTO.getPhone().trim());
            newUser.setAddress(userRequestDTO.getAddress());
            newUser.setAge(userRequestDTO.getAge());
            newUser.setPassword(userRequestDTO.getPassword());
            newUser.setCreatedBy(userRequestDTO.getName());
            Timestamp currentDate = new Timestamp(new Date().getTime());
            newUser.setCreatedTime(currentDate);
            newUser.setUpdatedBy(userRequestDTO.getName());
            newUser.setUpdatedTime(currentDate);
            newUser.setStatus(1);
            userRepository.save(newUser);
            return this.convertUser(newUser);
        }
    }

    @Override
    public String updateUserInfo(Integer userId, UserRequestDTO userRequestDTO) {
        Optional<User> userFromDB = userRepository.findById(userId);
        if (userFromDB == null) {
            return "FAIL";
        }
        User user1 = userFromDB.get();
        user1.setName(userRequestDTO.getName());
        user1.setGender(userRequestDTO.getGender());
        user1.setPhone(userRequestDTO.getPhone());
        user1.setAddress(userRequestDTO.getAddress());
        user1.setAge(userRequestDTO.getAge());
        user1.setUpdatedBy(userRequestDTO.getName());
        user1.setUpdatedTime(new Timestamp(new Date().getTime()));
        userRepository.save(user1);
        return "SUCCESS";
    }

    @Override
    public String updatePassword(String password, Integer userId, String passwordSaved) {
        Optional<User> userSaved = userRepository.findById(userId);
        if (!userSaved.isPresent()){
            return "FAIL";
        }
        User user = userSaved.get();

        if (passwordEncoder.matches(user.getPassword(),passwordSaved.trim()) || password.equals(passwordSaved)){
            return "FAIL";
        }
        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);
        return "SUCCESS";
    }

    @Override
    public UserDTO findUserById(Integer id) {
        User userSaved = userRepository.findUserByIdAndStatus(id,1);
        if (userSaved == null) {
            return null;
        }
        return this.convertUser(userRepository.findUserByIdAndStatus(id,1));
    }

    @Override
    public UserDTO findUserByPhone(String phone) {
        Optional<User> user = userRepository.findByPhone(phone,1);
        if (!user.isPresent()) {
            return null;
        }
        User userSaved = user.get();
        return this.convertUser(userSaved);
    }

    @Override
    public List<BillDTO> findBillsByUserId(Integer userId) {
        return billService.convertBills(bookingRepository.findBookingByUserId(userId));
    }

    @Override
    public List<BookingDTO> findBookingsByUserId(Integer userId) {
        return bookingService.convertBookings(bookingRepository.findBookingByUserId(userId));
    }

    @Override
    public List<UserDTO> findAllUserLoyalByStoreId(Integer storeId) {
        if (storeId == null) {
            return null;
        }
        List<User> userSaved = userRepository.findUsersLoyalByStoreId(storeId);
        if (userSaved.isEmpty()) {
            return null;
        }
        return this.convertUsers(userSaved);
    }

    @Override
    public List<UserDTO> filterUsers(UserRequestDTO userRequestDTO) {
        return this.convertUsers(userRepository.filterUsers(userRequestDTO));
    }

    @Override
    public UserDTO convertUser(User user) {
        return new UserDTO(user.getName(), user.getGender()
                , user.getPhone(), user.getAddress(), user.getAge(), user.getStatus(),
                storeService.convertStoresDTO(user.getStores()),
                this.findBillsByUserId(user.getId()),
                this.findBookingsByUserId(user.getId()));
    }

    @Override
    public List<UserDTO> convertUsers(List<User> users) {
        return users.stream().map(user -> new UserDTO(
                user.getName(), user.getGender()
                , user.getPhone(), user.getAddress(), user.getAge(), user.getStatus(),
                storeService.convertStoresDTO(user.getStores()),
                this.findBillsByUserId(user.getId()),
                this.findBookingsByUserId(user.getId())
        )).collect(Collectors.toList());
    }
}
