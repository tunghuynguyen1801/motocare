package com.example.quanlysuaxe.service.impl;

import com.example.quanlysuaxe.dto.requestDTO.BookingRequestDTO;
import com.example.quanlysuaxe.dto.responseDTO.BookingDTO;
import com.example.quanlysuaxe.entity.Booking;
import com.example.quanlysuaxe.entity.Store;
import com.example.quanlysuaxe.entity.User;
import com.example.quanlysuaxe.repository.BookingRepository;
import com.example.quanlysuaxe.repository.StoreRepository;
import com.example.quanlysuaxe.repository.UserRepository;
import com.example.quanlysuaxe.service.BookingService;
import com.example.quanlysuaxe.service.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BookingServiceImpl implements BookingService {
    @Autowired
    private BookingRepository bookingRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private StoreRepository storeRepository;
    @Autowired
    private ServiceService serviceService;


    @Override
    public String addBooking(BookingRequestDTO dto, Integer userId, Integer storeId) throws ParseException {
        User user = userRepository.findUserByIdAndStatus(userId, 1);
        Optional<Store> storeSaved = storeRepository.findByIdStore(storeId);
        if (user == null || !storeSaved.isPresent()) {
            return "FAIL";
        }
        Store store = storeSaved.get();
        Booking booking = new Booking();
        booking.setUser(user);
        booking.setStore(store);
        booking.setUserName(dto.getUserName());
        booking.setPhoneUser(dto.getUserPhone());
        booking.setTimeRepair(dto.getTimeRepair());
        booking.setNumberPlate(dto.getNumberPlate());
        //sẽ lấy ra user ở context khi họ gọi request = spring context của spring security
        booking.setCreatedBy(dto.getUserName());
        booking.setUpdatedBy(dto.getUserName());

        booking.setStatus(0);
        Timestamp currentDate = new Timestamp(new Date().getTime());
        booking.setUpdatedTime(currentDate);
        booking.setCreatedTime(currentDate);
        booking.setServices(serviceService.serviceDTOConvertService(dto.getServices()));
        bookingRepository.save(booking);
        return "SUCCESS";
    }

    @Override
    public String deleteBooking(Integer id) { //thay đổi trạng thái, ko xóa bay
        Optional<Booking> bookingSaved = bookingRepository.findById(id);
        if (!bookingSaved.isPresent()) {
            return "FAIL";
        }
        Booking booking = bookingSaved.get();
        bookingRepository.delete(booking);
        return "SUCCESS"; //không nên dùng hasCode như này -> dùng Const
    }

    @Override
    public String confirmBooking(Integer storeId, Integer bookingId) {
        Optional<Booking> bookingSaved = bookingRepository.findById(bookingId);
        if (!bookingSaved.isPresent()) {
            return "Fail";
        }
        Optional<Store> storeSaved = storeRepository.findById(storeId);
        if (!storeSaved.isPresent()) {
            return "Fail";
        }
        Booking booking = bookingSaved.get();
        booking.setStatus(1);
        booking.setUpdatedBy(storeSaved.get().getUser().getName());
        booking.setUpdatedTime(new Timestamp(new Date().getTime()));
        bookingRepository.save(booking);
        return "SUCCESS";
    }

    @Override
    public List<BookingDTO> convertBookings(List<Booking> bookings) {
        List<BookingDTO> result = new ArrayList<>();
        for (Booking b : bookings) { //hạn chế dùng query trong vòng for || dùng cấu trúc dữ liệu theo kiểu map
            if (b.getStatus().equals(0)) {
                result.add(new BookingDTO(b.getId(), b.getUserName(), b.getPhoneUser(),
                        b.getStore().getName(), b.getTimeRepair(), b.getNumberPlate(),
                        storeRepository.findServicesByBookingId(b.getId()),
                        b.getCreatedTime()));
            }
        }
        return result;
    }

    @Override
    public List<BookingDTO> filterBookings(String userName, String phoneUser, Integer storeId) {
        return this.convertBookings(storeRepository.filterBookings(userName, phoneUser, storeId));
    }
}
