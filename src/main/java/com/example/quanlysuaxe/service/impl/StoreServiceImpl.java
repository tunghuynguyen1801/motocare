package com.example.quanlysuaxe.service.impl;

import com.example.quanlysuaxe.dto.requestDTO.ServiceRequestDTO;
import com.example.quanlysuaxe.dto.requestDTO.StoreRequestDTO;
import com.example.quanlysuaxe.dto.responseDTO.BookingDTO;
import com.example.quanlysuaxe.dto.responseDTO.StoreDTO;
import com.example.quanlysuaxe.entity.Service;
import com.example.quanlysuaxe.entity.Store;
import com.example.quanlysuaxe.repository.ServiceRepository;
import com.example.quanlysuaxe.repository.StoreRepository;
import com.example.quanlysuaxe.repository.UserRepository;
import com.example.quanlysuaxe.service.BookingService;
import com.example.quanlysuaxe.service.CommentService;
import com.example.quanlysuaxe.service.ServiceService;
import com.example.quanlysuaxe.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service
public class StoreServiceImpl implements StoreService {
    @Autowired
    private StoreRepository storeRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ServiceService serviceService;
    @Autowired
    private ServiceRepository serviceRepository;
    @Autowired
    private CommentService commentService;
    @Autowired
    private BookingService bookingService;

    @Override
    public List<BookingDTO> findBookingsByStoreId(Integer storeId, String startDate, String endDate) {
        return bookingService.convertBookings(storeRepository.findBookingsByStoreId(storeId, startDate, endDate));
    }

    @Override
    public StoreDTO getStoreById(Integer storeId) {
        Optional<Store> storeCheck = (storeRepository.findByIdStore(storeId));
        if (!storeCheck.isPresent()) {
            return null;
        }
        Store store = storeCheck.get();
        return this.convertStore(store);
    }

    @Override
    public StoreDTO editStore(StoreRequestDTO storeDTO, Integer storeId) {
        Optional<Store> store = (storeRepository.findByIdStore(storeId));
        if (!store.isPresent()) {
            return null;
        }
        Store newStore = store.get();
        newStore.setName(storeDTO.getName());
        newStore.setAddress(storeDTO.getAddress());
        newStore.setPhone(storeDTO.getPhone());
        newStore.setStatus(newStore.getStatus());

        return this.convertStore(storeRepository.save(newStore));
    }

    @Override
    public String deleteStore(Integer id) {
        try {
            Optional<Store> store = storeRepository.findByIdStore(id);
            Store store1 = store.get();
            storeRepository.delete(store1);
        } catch (Exception e) {
            return e.getMessage();
        }
        return "Delete Successfully";
    }

    public Set<Service> getServiceSaved(List<Service> services, Set<ServiceRequestDTO> requestDTO) {
        Set<Service> listService = new HashSet<>();
        for (ServiceRequestDTO dto : requestDTO) {
            for (Service s : services) {
                ServiceRequestDTO newDTO = new ServiceRequestDTO(s.getName(), s.getPrice(), s.getTypeVehicle());
                if (dto.equals(newDTO)) {
                    listService.add(s);
                    break;
                }
            }
        }
        return listService;
    }

    public Boolean checkService(List<Service> services, ServiceRequestDTO requestDTO) { //dùng câu query
        Set<ServiceRequestDTO> serviceList = services.stream().map(
                        service -> new ServiceRequestDTO(
                                service.getName(), service.getPrice(), service.getTypeVehicle()))
                .collect(Collectors.toSet());
        if (serviceList.contains(requestDTO)) {
            return true;
        }
        return false;
    }

    @Override
    public String addNewService(ServiceRequestDTO serviceRequestDTO, Integer storeId) {
        Optional<Store> store = storeRepository.findByIdStore(storeId);
        Store store1 = store.get();
        Date datTime = new Date();
        Set<Service> services = store1.getServices();
        Set<ServiceRequestDTO> serviceList = services
                .stream().map(service -> new ServiceRequestDTO(service.getName(),
                        service.getPrice(), service.getTypeVehicle()))
                .collect(Collectors.toSet());
        if (serviceList.contains(serviceRequestDTO)) {
            return "Fail";
        }

        if (!checkService(serviceRepository.findServicesByName(serviceRequestDTO.getName()), serviceRequestDTO)) {
            Service service = serviceRepository.save(new Service
                    (null, serviceRequestDTO.getName(),
                            serviceRequestDTO.getPrice(),
                            "Nguyen Huy Tùng", new Timestamp(datTime.getTime()),
                            "TEAM BE", new Timestamp(datTime.getTime()),
                            1, serviceRequestDTO.getTypeVehicle()));
            services.add(service);
        }
        Set<ServiceRequestDTO> dto = new HashSet<>();
        dto.add(serviceRequestDTO);
        Set<Service> listService =
                getServiceSaved(serviceRepository.findServicesByName(serviceRequestDTO.getName()), dto);
        services.addAll(listService);
        storeRepository.save(store1);
        return "success";
    }

    @Override
    public String deleteService(Integer storeId, Integer serviceId) {
        try {
            serviceRepository.deleteServiceStore(storeId, serviceId);
            if (!serviceRepository.findServiceById(serviceId).isPresent()) {
                return "Delete successfully";
            }
        } catch (Exception e) {
            return e.getMessage();
        }
        return "Delete false";
    }

    @Override
    public String updateService(ServiceRequestDTO serviceRequestDTO, Integer serivceId, Integer storeId) {
        Optional<Service> serviceSaved = serviceRepository.findServiceById(serivceId);
        Service service1 = serviceSaved.get();
        ServiceRequestDTO serviceDTOSaved = new ServiceRequestDTO(service1.getName(),
                service1.getPrice(), service1.getTypeVehicle());
        if (serviceDTOSaved.equals(serviceRequestDTO)) {
            return "Fail";
        }
        Optional<Store> store = storeRepository.findByIdStore(storeId);
        Store store1 = store.get();
        Date datTime = new Date();

        String nameService = serviceRequestDTO.getName();
        Set<Service> services = store1.getServices();

        //update service cho chính service
        if (!checkService(serviceRepository.findServicesByName(nameService), serviceRequestDTO)) {
            Service service = serviceRepository.save(new Service
                            (null, serviceRequestDTO.getName(),
                                    serviceRequestDTO.getPrice(),
                                    "TEAM BE", new Timestamp(datTime.getTime()),
                                    "TEAM BE", new Timestamp(datTime.getTime()),
                                    1, serviceRequestDTO.getTypeVehicle()));
            services.add(service);
        }

        //update service cho store
        Set<ServiceRequestDTO> dtoService = new HashSet<>();
        dtoService.add(serviceRequestDTO);

        services.addAll(getServiceSaved(serviceRepository.findServicesByName(nameService),
                dtoService));
        storeRepository.save(store1);
        serviceRepository.deleteServiceStore(storeId, serivceId);
        return "success";
    }

    @Override
    public List<StoreDTO> convertStoresDTO(List<Store> stores) {
        List<StoreDTO> result = stores
                .stream().map(store -> new StoreDTO(store.getId(), store.getName(),
                        store.getAddress(), store.getPhone(),
                        store.getStatus(),
                        serviceService.convertServices(store.getServices()),
                        commentService.convertComments(store.getComments())
                )).collect(Collectors.toList());
        return result;
    }

    @Override
    public StoreDTO convertStore(Store store) {
        return new StoreDTO(store.getId(), store.getName(),
                store.getAddress(), store.getPhone(),
                store.getStatus(),
                serviceService.convertServices(store.getServices()),
                commentService.convertComments(store.getComments()));
    }
}
