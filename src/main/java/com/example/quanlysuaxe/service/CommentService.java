package com.example.quanlysuaxe.service;

import com.example.quanlysuaxe.dto.requestDTO.CommentRequestDTO;
import com.example.quanlysuaxe.dto.responseDTO.CommentDTO;
import com.example.quanlysuaxe.entity.Comment;

import java.util.List;

public interface CommentService {
    String insertComment(int userId, int storeId, CommentRequestDTO dto);

    List<CommentDTO> convertComments(List<Comment> comments);

    List<CommentDTO> getListComments(Integer storeId);
}
