package com.example.quanlysuaxe.service;

import com.example.quanlysuaxe.dto.responseDTO.BillDTO;
import com.example.quanlysuaxe.entity.Booking;

import java.util.List;

public interface BillService {
    List<BillDTO> convertBills(List<Booking> bookings);
}
