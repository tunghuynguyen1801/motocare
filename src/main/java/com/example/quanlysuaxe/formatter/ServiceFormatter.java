package com.example.quanlysuaxe.formatter;

import com.example.quanlysuaxe.entity.Service;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Locale;

@Component
public class ServiceFormatter implements Formatter<Service> {
    @Override
    public Service parse(String serviceId, Locale locale) throws ParseException {
        Service service = new Service();
        service.setId(Integer.parseInt(serviceId));
        return service;
    }

    @Override
    public String print(Service object, Locale locale) {
        return String.valueOf(object.getId());
    }
}
