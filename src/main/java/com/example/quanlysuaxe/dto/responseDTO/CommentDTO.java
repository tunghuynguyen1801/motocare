package com.example.quanlysuaxe.dto.responseDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentDTO {
    private String userName;
    private int numberStar;
    private String content;
    private Timestamp createdTime;
}
