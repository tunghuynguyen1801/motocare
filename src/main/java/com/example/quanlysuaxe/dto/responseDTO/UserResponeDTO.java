package com.example.quanlysuaxe.dto.responseDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserResponeDTO {
    private Integer userId;
    private String name;
    private String gender;
    private String phone;
    private String address;
    private Integer age;
    private Integer status;
    private List<StoreDTO> storeList;
}
