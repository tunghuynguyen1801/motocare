package com.example.quanlysuaxe.dto.responseDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    private String name;
    private String gender;
    private String phone;
    private String address;
    private Integer age;
    private Integer status;
    private List<StoreDTO> storeList;
    private List<BillDTO> billList;
    private List<BookingDTO> bookingList;
}

