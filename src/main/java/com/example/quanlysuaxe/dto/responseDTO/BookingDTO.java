package com.example.quanlysuaxe.dto.responseDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookingDTO {
    private Integer bookingId;

    private String userName;

    private String phoneUser;

    private String storeName;

    private Timestamp timeRepair;

    private String numberPlate;

    private List<ServiceDTO> serviceDTOList;

    private Timestamp created_time;
}
