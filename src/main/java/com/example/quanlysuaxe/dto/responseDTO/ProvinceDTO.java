package com.example.quanlysuaxe.dto.responseDTO;


import lombok.Data;

@Data
public class ProvinceDTO {
    private String name;

    public ProvinceDTO(String name) {
        this.name = name;
    }
}
