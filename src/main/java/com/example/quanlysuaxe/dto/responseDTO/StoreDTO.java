package com.example.quanlysuaxe.dto.responseDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class StoreDTO {
    private Integer storeId;
    private String name;
    private String address;
    private String phone;
    private Integer status;
    private Set<ServiceDTO> serviceList;
    private List<CommentDTO> comments;
}
