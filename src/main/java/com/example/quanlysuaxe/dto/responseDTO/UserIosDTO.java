package com.example.quanlysuaxe.dto.responseDTO;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserIosDTO {
    private Integer userId;
    private String name;
    private String gender;
    private String phone;
    private String address;
    private Integer age;
    private Integer status;
}
