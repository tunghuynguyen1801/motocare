package com.example.quanlysuaxe.dto.responseDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ServiceDTO {
    private Integer serviceId;
    private String name;
    private double price;
    private String typeVehicle;
}
