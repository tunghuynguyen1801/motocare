package com.example.quanlysuaxe.dto.requestDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentRequestDTO {
    @Max(value = 5)
    private int star;
    private String content;
}
