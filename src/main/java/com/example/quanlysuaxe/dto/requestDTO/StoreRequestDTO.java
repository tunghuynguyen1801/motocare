package com.example.quanlysuaxe.dto.requestDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreRequestDTO {
    private String name;
    private String address;
//    private String type;
    private String phone;
    private Integer pageIndex;
    private Integer pageSize;
    private Integer status;
    private Set<ServiceRequestDTO> services;


}
