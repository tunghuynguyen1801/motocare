package com.example.quanlysuaxe.dto.requestDTO;


import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRequestDTO {

    private String name;

    private String gender;

    @NotNull
    private String phone;

    private String address;

    private Integer age;

    @Column(name = "password", nullable = false, length = 45)
    private String password;

    private int role;

    private int status;
}
