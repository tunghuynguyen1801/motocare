package com.example.quanlysuaxe.dto.requestDTO;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
public class PasswordRequest {
    @NotBlank
    private String passwordSaved;

    @NotBlank
    private String newPassword;
}
