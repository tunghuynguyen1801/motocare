package com.example.quanlysuaxe.dto.requestDTO;

import com.example.quanlysuaxe.dto.responseDTO.ServiceDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookingRequestDTO {
    private String userName; //chuyển thành userId
    private String userPhone;
    private String storeName; //chuyển thành storeId
    private Timestamp timeRepair;
    private String numberPlate;
    private Set<ServiceDTO> services ;
}
