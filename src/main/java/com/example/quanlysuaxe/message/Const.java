package com.example.quanlysuaxe.message;

public class Const {

    public static final String SUCCESS = "SUCCESS";
    public static final String ERROR = "ERROR";
    public static final String FAIL = "FAIL";

    public static class API_RESPONSE {
        private API_RESPONSE() {
            throw new IllegalStateException();
        }
        public static final String RETURN_CODE_SUCCESS = "200";
        public static final String RETURN_CODE_ERROR = API_RESPONSE.RETURN_CODE_ERROR;

        public static final Boolean STATUS_TRUE = true;
        public static final Boolean STATUS_FALSE = false;
    }

}
